import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kudos/enum/user_state.dart';
import 'package:kudos/models/message.dart';
import 'package:kudos/models/users.dart';
import 'firebase_methods.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseRepository {
  FirebaseMethods _firebaseMethods = FirebaseMethods();

  ///method is responsible for getting the currently logged in user details
  Future<FirebaseUser> getCurrentUser() => _firebaseMethods.getCurrentUser();

  ///responsible for authenticating a user
  Future<bool> authenticateUser(FirebaseUser user) =>
      _firebaseMethods.authenticateUser(user);

  ///responsible for signing out
  Future<void> signOut() => _firebaseMethods.signOut();

  ///responsible for adding the data to db
  Future<void> addDataToDb(FirebaseUser user) =>
      _firebaseMethods.addDataToDb(user);

  ///responsible for signing in
  Future<FirebaseUser> signIn() => _firebaseMethods.signIn();

  ///fetches the user details by their id
  Future<User> fetchUserDetailsById(String uid) =>
      _firebaseMethods.fetchUserDetailsById(uid);

  ///fetches all the users that are present in the users collection
  Future<List<User>> fetchAllUsers(FirebaseUser user) =>
      _firebaseMethods.fetchAllUsers(user);

  ///responsible for making necessary changes in the database while commencing a video call
  Future<void> commenceVideoCall(
          String receiverId, String callerId, String channelId) =>
      _firebaseMethods.commenceVideoCall(receiverId, callerId, channelId);

  ///responsible for making necessary changes in the database while ending a video call
  Future<void> endVideoCall(String receiverId, String callerId) =>
      _firebaseMethods.endVideoCall(receiverId, callerId);

  Future<void> engageUsers(String receiverId, String callerId) =>
      _firebaseMethods.engageUsers(receiverId, callerId);

  Future<DocumentSnapshot> getCallerDetails(String receiverId) =>
      _firebaseMethods.getCallerDetails(receiverId);

  Future<void> addMessageToDb(Message message, User sender, User receiver) =>
      _firebaseMethods.addMessageToDb(
          message, sender, receiver);

  Future<String> getLastMessage(String currentUserId, String contactId) =>
      _firebaseMethods.getLastMessage(currentUserId, contactId);

  Future<bool> checkIfUserExists(String phoneNumber) =>
      _firebaseMethods.checkIfUserExists(phoneNumber);

  Future<String> uploadImageToStorage(File imageFile) =>
      _firebaseMethods.uploadImageToStorage(imageFile);

  void showLoading(String receiverId, String senderId) =>
      _firebaseMethods.showLoading(receiverId, senderId);
  void hideLoading(String receiverId, String senderId) =>
      _firebaseMethods.hideLoading(receiverId, senderId);

  void uploadImageMsgToDb(String url, String receiverId, String senderId) =>
      _firebaseMethods.uploadImageMsgToDb(url, receiverId, senderId);

  Future<void> addCallDetailsToDb(
          Message message, String receiverId, String senderId, bool calling) =>
      _firebaseMethods.addCallDetailsToDb(message, receiverId, senderId, calling);

  void setUserState(String userId, UserState userState) =>
      _firebaseMethods.setUserState(userId, userState);
}
