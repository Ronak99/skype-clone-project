import 'dart:async';
import 'dart:io' as io;
import 'package:intl/intl.dart';
import 'package:kudos/models/call.dart';
import 'package:kudos/models/users.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path_provider/path_provider.dart';

class SqliteMethods {
  static Database _db;

  //database name
  static const String DB_NAME = 'LogDB';

  static const String TABLENAME = 'CallLogs';

  //columns
  static const String ID = 'id';
  static const String RECEIVER_ID = 'receiver_id';
  static const String RECEIVER_NAME = 'receiver_name';
  static const String STATUS = 'status';
  static const String TIME = 'time';
  static const String RECEIVER_PHOTO = 'receiver_photo';

  Future<Database> get db async {
    if (_db != null) {
      // print("db is not null");
      return _db;
    }
    print("db was null, now awaiting it");
    _db = await initDb();
    return _db;
  }

  initDb() async {
    print("inside of initdb");
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, DB_NAME);
    var db = await openDatabase(path, version: 5, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int version) async {
    String createTableQuery =
        "CREATE TABLE $TABLENAME ($ID INTEGER PRIMARY KEY, $RECEIVER_ID TEXT, $RECEIVER_NAME TEXT, $STATUS TEXT, $TIME TEXT, $RECEIVER_PHOTO TEXT)";

    await db.execute(createTableQuery);
    print("creating table");
  }

  Future<Call> addLogs(User contact, String callStatus) async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('EEEE, hh:mm a').format(now);

    Call call = Call(
        id: null,
        receiverId: contact.uid,
        receiverName: contact.name,
        receiverPhoto: contact.profilePhoto,
        status: callStatus,
        time: formattedDate);

    print("inside of add logs fn");
    var dbClient = await db;
    call.id = await dbClient.insert(TABLENAME, call.toMap());
    print(call.id.toString() + ", entry in sqlite has been done");
    return call;
  }

  Future<void> deleteLog(int id) async {
    var client = await db;
    return await client.delete(TABLENAME, where: '$ID = ?', whereArgs: [id]);
  }

  Future<List<Call>> getLogs() async {
    var dbClient = await db;
    List<Map> maps = await dbClient.query(TABLENAME, columns: [
      ID,
      RECEIVER_ID,
      RECEIVER_NAME,
      STATUS,
      TIME,
      RECEIVER_PHOTO
    ]);
    //List<Map> maps = await dbClient.rawQuery("SELECT * FROM $TABLE");
    List<Call> logList = [];
    if (maps.length > 0) {
      for (int i = 0; i < maps.length; i++) {
        logList.insert(0, Call.fromMap(maps[i]));
      }
    }
    return logList;
  }

  Future close() async {
    var dbClient = await db;
    dbClient.close();
  }
}
