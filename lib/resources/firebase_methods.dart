import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kudos/enum/user_state.dart';
import 'package:kudos/models/message.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/utils/utilities.dart';

class FirebaseMethods {
  //library related variables
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  static final Firestore _firestore = Firestore.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final CollectionReference _userCollection = _firestore.collection("users");
  Message _message;
  StorageReference _storageReference;

  //file related variables
  User user = User();

  //gets current user
  Future<FirebaseUser> getCurrentUser() async {
    FirebaseUser currentUser;
    currentUser = await _auth.currentUser();
    return currentUser;
  }

  void addThousandUsers() {
    for (int i = 0; i < 500; i++) {
      String uid = "userno" + i.toString();
      String name = "user" + i.toString();
      User user = User(
        uid: uid,
        email: "someemail.com",
        name: name,
        profilePhoto:
            "https://cdn.iconverticons.com/files/png/1c3a93787b4867ca_256x256.png",
        username: name,
      );
      _firestore.collection("users").document(uid).setData(user.toMap(user));
    }
    print("all done");
  }

  Future<void> addDataToDb(FirebaseUser currentUser) async {
    print("Inside addDataToDb Method");

    String username = Utils.getUsername(currentUser.email);
    int defaultState = Utils.stateToNum(UserState.Online);

    user = User(
        uid: currentUser.uid,
        email: currentUser.email,
        name: currentUser.displayName,
        profilePhoto: currentUser.photoUrl,
        state: defaultState,
        username: username);

    _firestore
        .collection("users")
        .document(currentUser.uid)
        .setData(user.toMap(user));
  }

  Future<bool> authenticateUser(FirebaseUser user) async {
    print("Inside authenticateUser");
    final QuerySnapshot result = await _firestore
        .collection("users")
        .where("email", isEqualTo: user.email)
        .getDocuments();

    final List<DocumentSnapshot> docs = result.documents;

    return docs.length == 0 ? true : false;
  }

  //sign out method
  Future<void> signOut() async {
    print("inside signout function");
    await _googleSignIn.disconnect();
    await _googleSignIn.signOut();
    return await _auth.signOut();
  }

  //sign in method
  Future<FirebaseUser> signIn() async {
    GoogleSignInAccount _signInAccount = await _googleSignIn.signIn();
    GoogleSignInAuthentication _signInAuthentication =
        await _signInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: _signInAuthentication.accessToken,
      idToken: _signInAuthentication.idToken,
    );

    final FirebaseUser user = await _auth.signInWithCredential(credential);
    return user;
  }

  Future<List<DocumentSnapshot>> retrievePosts(FirebaseUser user) async {
    List<DocumentSnapshot> list = List<DocumentSnapshot>();
    List<DocumentSnapshot> updatedList = List<DocumentSnapshot>();
    QuerySnapshot querySnapshot;
    QuerySnapshot snapshot =
        await _firestore.collection("users").getDocuments();
    for (int i = 0; i < snapshot.documents.length; i++) {
      if (snapshot.documents[i].documentID != user.uid) {
        list.add(snapshot.documents[i]);
      }
    }
    for (var i = 0; i < list.length; i++) {
      querySnapshot =
          await list[i].reference.collection("posts").getDocuments();
      for (var i = 0; i < querySnapshot.documents.length; i++) {
        updatedList.add(querySnapshot.documents[i]);
      }
    }
    // fetchSearchPosts(updatedList);
    print("UPDATED LIST LENGTH : ${updatedList.length}");
    return updatedList;
  }

  Future<User> fetchUserDetailsById(String uid) async {
    DocumentSnapshot documentSnapshot =
        await _firestore.collection("users").document(uid).get();
    return User.fromMap(documentSnapshot.data);
  }

  Future<List<User>> fetchAllUsers(FirebaseUser user) async {
    List<User> userList = List<User>();
    QuerySnapshot querySnapshot =
        await _firestore.collection("users").getDocuments();
    for (var i = 0; i < querySnapshot.documents.length; i++) {
      if (querySnapshot.documents[i].documentID != user.uid) {
        userList.add(User.fromMap(querySnapshot.documents[i].data));
      }
    }
    print("USERSLIST : ${userList.length}");
    return userList;
  }

  //changes the call_to and call_by, and sets channel_name for both caller and receiver
  Future<void> commenceVideoCall(
      String receiverId, String callerId, String channelId) async {
    //steps involved in a video call
    //first change the callto from the caller,
    _userCollection
        .document(callerId)
        .updateData({"call_to": receiverId}).then((_) {
      _userCollection.document(receiverId).updateData({"call_by": callerId});
    });

    //generate a random channel id and set it for the caller
    _userCollection.document(callerId).updateData({"channel_name": channelId});
    //simultaneously set it for the receiver as well.
    _userCollection
        .document(receiverId)
        .updateData({"channel_name": channelId});
  }

  Future<void> endVideoCall(String receiverId, String callerId) async {
    _userCollection.document(callerId).updateData({"call_to": null});
    _userCollection.document(receiverId).updateData({"call_by": null});
    _userCollection.document(callerId).updateData({"channel_name": null});
    _userCollection.document(receiverId).updateData({"channel_name": null});

    //setting the engaged to false for both the users
    _userCollection.document(receiverId).updateData({"engaged": false});
    _userCollection.document(callerId).updateData({"engaged": false});
  }

  Future<void> engageUsers(String receiverId, String callerId) async {
    _userCollection.document(receiverId).updateData({"engaged": true});
    _userCollection.document(callerId).updateData({"engaged": true});
  }

  Future<DocumentSnapshot> getCallerDetails(String receiverId) async {
    return await _userCollection.document(receiverId).get();
  }

  Future<void> addMessageToDb(
      Message message, User sender, User receiver) async {
    print("Message : ${message.message}");
    var map = message.toMap();

    print("Map : $map");

    //we need to cehck, if the sender is sending this
    //message for the very first time to a specific receiver

    await _firestore
        .collection("messages")
        .document(message.senderId)
        .collection(receiver.uid)
        .add(map);

    addToContacts(sender, receiver);

    return _firestore
        .collection("messages")
        .document(receiver.uid)
        .collection(message.senderId)
        .add(map);
  }

  void setUserState(String userId, UserState userState) {
    int stateNum = Utils.stateToNum(userState);

    _userCollection.document(userId).updateData({"state": stateNum});
  }

  Future<void> addCallDetailsToDb(
      Message message, String receiverId, String senderId, bool calling) async {
    var map = message.toMap();

    return calling
        ? await _firestore
            .collection("messages")
            .document(message.senderId)
            .collection(receiverId)
            .add(map)
        : _firestore
            .collection("messages")
            .document(receiverId)
            .collection(message.senderId)
            .add(map);
  }

  void addToContacts(User sender, User receiver) {
    _userCollection
        .document(sender.uid)
        .collection("contacts")
        .document(receiver.uid)
        .get()
        .then((doc) async {
      if (!doc.exists) {
        await _userCollection
            .document(sender.uid)
            .collection("contacts")
            .document(receiver.uid)
            .setData({
          "contact_id": receiver.uid,
          "contact_name": receiver.name,
          "contact_photo": receiver.profilePhoto
        });
      }
    });

    _userCollection
        .document(receiver.uid)
        .collection("contacts")
        .document(sender.uid)
        .get()
        .then((doc) async {
      if (!doc.exists) {
        await _userCollection
            .document(receiver.uid)
            .collection("contacts")
            .document(sender.uid)
            .setData({
          "contact_id": sender.uid,
          "contact_name": sender.name,
          "contact_photo": sender.profilePhoto
        });
      }
    });
  }

  void addReceiverToSenderContacts(String receiverId, String senderId,
      String receiverName, String receiverPhoto) {
    _userCollection
        .document(senderId)
        .collection("contacts")
        .document(receiverId)
        .get()
        .then((doc) async {
      if (!doc.exists) {
        await _userCollection
            .document(senderId)
            .collection("contacts")
            .document(receiverId)
            .setData({
          "contact_id": receiverId,
          "contact_name": receiverName,
          "contact_photo": receiverPhoto
        });
        print("done");
      }
    });
  }

  Future<String> getLastMessage(String currentUserId, String contactId) async {
    String lastMessage = "";

    QuerySnapshot snapshot = await Firestore.instance
        .collection("messages")
        .document(currentUserId)
        .collection(contactId)
        .getDocuments();

    lastMessage = snapshot.documents.last.data["message"];

    return lastMessage;
  }

  Future<bool> checkIfUserExists(String phoneNumber) async {
    bool userExists = false;

    QuerySnapshot snapshot = await _userCollection
        .where("phone", isEqualTo: phoneNumber)
        .getDocuments();

    if (snapshot.documents.first.data != null) {
      userExists = true;
    }

    return userExists;
  }

  Future<String> uploadImageToStorage(File imageFile) async {
    _storageReference = FirebaseStorage.instance
        .ref()
        .child('${DateTime.now().millisecondsSinceEpoch}');
    StorageUploadTask storageUploadTask = _storageReference.putFile(imageFile);
    var url = await (await storageUploadTask.onComplete).ref.getDownloadURL();
    return url;
  }

  void uploadImageMsgToDb(String url, String receiverId, String senderId) {
    _message = Message.imageMessage(
        message: "IMAGE",
        receiverId: receiverId,
        senderId: senderId,
        photoUrl: url,
        timestamp: FieldValue.serverTimestamp(),
        type: 'image');
    var map = Map<String, dynamic>();
    map['message'] = _message.message;
    map['senderId'] = _message.senderId;
    map['receiverId'] = _message.receiverId;
    map['type'] = _message.type;
    map['timestamp'] = _message.timestamp;
    map['photoUrl'] = _message.photoUrl;

    _firestore
        .collection("messages")
        .document(_message.senderId)
        .collection(receiverId)
        .add(map)
        .whenComplete(() {
      print("Messages added to db");
    });

    _firestore
        .collection("messages")
        .document(receiverId)
        .collection(_message.senderId)
        .add(map)
        .whenComplete(() {
      print("Messages added to db");
    });
  }

  void showLoading(String receiverId, String senderId) async {
    _message = Message(
        message: "Uploading Image...",
        receiverId: receiverId,
        senderId: senderId,
        timestamp: FieldValue.serverTimestamp(),
        type: 'loading');
    var map = Map<String, dynamic>();
    map['message'] = _message.message;
    map['senderId'] = _message.senderId;
    map['receiverId'] = _message.receiverId;
    map['type'] = _message.type;
    map['timestamp'] = _message.timestamp;

    _firestore
        .collection("messages")
        .document(_message.senderId)
        .collection(receiverId)
        .document("loadingDoc")
        .setData(map)
        .whenComplete(() {
      print("Messages added to db");
    });
  }

  void hideLoading(String receiverId, String senderId) {
    _firestore
        .collection("messages")
        .document(_message.senderId)
        .collection(receiverId)
        .document("loadingDoc")
        .delete();
  }
}
