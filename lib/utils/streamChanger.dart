import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/theme/universal_colors.dart';

class StreamChanger {
  String currentUserId;
  final CallBloc bloc = UniversalVariables.bloc;
  FirebaseRepository _repository = FirebaseRepository();

  void changeStream() {
    _repository.getCurrentUser().then((user) {
      currentUserId = user.uid;
    });

    Firestore.instance
        .collection('users')
        .document(currentUserId)
        .snapshots()
        .listen((doc) {
      //if channel name becomes a string, it means that someone is calling you.
      if (doc.exists && doc.data != null) {
        if (doc["channel_name"] is String && doc["call_by"] is String) {
          bloc.incoming();
        } else if (doc["channel_name"] is String && doc["call_to"] is String) {
          bloc.dialled();
        } else {
          bloc.rejected();
        }
      }
    });
  }
}
