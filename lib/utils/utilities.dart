import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_ringtone_player/flutter_ringtone_player.dart';
import './../enum/call_state.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/enum/user_state.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/screens/callscreens/call.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:random_string/random_string.dart';

class Utils {
  static String incomingTune = "https://mobcup.net/d/o3drz01f/mp3";

  static String getUsername(String mail) {
    return ("live:${mail.split('@')[0]}");
  }

  static String generateRandomString() {
    return randomAlphaNumeric(10);
  }

  static String getInitials(String name) {
    List<String> nameSplit = name.split(" ");
    String firstNameInitial = nameSplit[0][0];
    String lastNameInitial = nameSplit[1][0];
    return firstNameInitial + lastNameInitial;
  }

  static void playRingtoneOnCall(AsyncSnapshot<CallState> snapshot, playing) {
    switch (snapshot.data) {
      case CallState.INCOMING:
        FlutterRingtonePlayer.play(
          android: AndroidSounds.ringtone,
          ios: IosSounds.alarm,
          looping: true,
          volume: 0.3,
        );
        playing = true;
        break;

      case CallState.REJECTED:
        if (playing == true) FlutterRingtonePlayer.stop();
        break;

      case CallState.PICKED:
        if (playing == true) FlutterRingtonePlayer.stop();
        break;

      default:
        if (playing == true) FlutterRingtonePlayer.stop();
    }
  }

  void commenceVideoCall(BuildContext context, String senderId, User receiver) {
    final CallBloc bloc = UniversalVariables.bloc;
    FirebaseRepository _repository = FirebaseRepository();

    handleCameraAndMicrophonePermissions().then((permissionGranted) {
      if (permissionGranted) {
        String channelId = Utils.generateRandomString();

        //change the state of the call
        bloc.dialled();

        //then we make changes to the database using this function
        _repository.commenceVideoCall(receiver.uid, senderId, channelId);

        //then we move on to the callpage
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CallPage(
                callerId: senderId,
                receiverId: receiver.uid,
                channelName: channelId,
                calling: true,
                contactName: receiver.name,
                contactPhoto: receiver.profilePhoto,
              ),
            ));
      }
    });

    //first, we wait for the user to grant us the necessary permissions
  }

  static int stateToNum(UserState userState) {
    switch (userState) {
      case UserState.Offline:
        return 0;
        break;

      case UserState.Online:
        return 1;
        break;

      default:
        return 2;
    }
  }

  static UserState numToState(int number) {
    switch (number) {
      case 0:
        return UserState.Offline;
        break;

      case 1:
        return UserState.Online;
        break;

      default:
        return UserState.Waiting;
        break;
    }
  }

  Future<bool> handleCameraAndMicrophonePermissions() async {
    PermissionStatus cameraPermissionStatus = await _getCameraPermission();
    PermissionStatus microphonePermissionStatus =
        await _getMicrophonePermission();

    if (cameraPermissionStatus == PermissionStatus.granted &&
        microphonePermissionStatus == PermissionStatus.granted) {
      return true;
    } else {
      _handleInvalidPermissions(
          cameraPermissionStatus, microphonePermissionStatus);
      return false;
    }
  }

  Future<PermissionStatus> _getCameraPermission() async {
    PermissionStatus permission =
        await PermissionHandler().checkPermissionStatus(PermissionGroup.camera);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.camera]);
      return permissionStatus[PermissionGroup.camera] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  Future<PermissionStatus> _getMicrophonePermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.microphone);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.microphone]);
      return permissionStatus[PermissionGroup.microphone] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  void _handleInvalidPermissions(
    PermissionStatus cameraPermissionStatus,
    PermissionStatus microphonePermissionStatus,
  ) {
    if (cameraPermissionStatus == PermissionStatus.denied &&
        microphonePermissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (cameraPermissionStatus == PermissionStatus.disabled &&
        microphonePermissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }

  static String getTimeFromNumber(num) {
    //since we want the time back in second
    int hours = (num / 3600).floor();
    int minutes = (num / 60).floor();
    int seconds = num % 60;

    if (hours > 0)
      return hours.toString() +
          "h " +
          minutes.toString() +
          "m " +
          seconds.toString() +
          "s";
    if (minutes > 0)
      return minutes.toString() + "m " + seconds.toString() + "s";

    return seconds.toString() + "s";
  }

  static void handleRintone(snapshot) {
    switch (snapshot.data) {
      case CallState.INCOMING:
        FlutterRingtonePlayer.play(
          android: AndroidSounds.ringtone,
          ios: IosSounds.alarm,
          looping: true,
          volume: 0.3,
        );
        break;

      default:
        FlutterRingtonePlayer.stop();
    }
  }

  static Icon getIconFromStatus(String callStatus) {
    Icon callIcon;
    double _iconSize = 15;
    Color _iconColor = UniversalVariables.greyColor;

    switch (callStatus) {
      case "MISSED":
        callIcon = Icon(
          Icons.call_missed,
          color: _iconColor,
          size: _iconSize,
        );
        break;

      case "CALLED":
        callIcon = Icon(
          Icons.call_made,
          color: _iconColor,
          size: _iconSize,
        );
        break;

      case "GOTCALLED":
        callIcon = Icon(
          Icons.call_received,
          color: _iconColor,
          size: _iconSize,
        );
        break;

      default:
        callIcon = Icon(Icons.camera);
    }
    return callIcon;
  }
}
