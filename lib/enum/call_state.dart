enum CallState{
  PICKED,
  REJECTED,
  DIALLED,
  INCOMING,
  MISSED,
  IDLE
}