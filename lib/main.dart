import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/bloc/duration_counter.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/screens/login_screen.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'screens/home_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './screens/search_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> with WidgetsBindingObserver {
  var _repository = FirebaseRepository();

  int backPressed = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
 
    return BlocProvider(
      blocs: [
        //add yours BLoCs controlles
        Bloc((i) => CallBloc()),
        Bloc((i) => DurationCounter()),
      ],
      child: MaterialApp(
          title: 'Skype Clone',
          debugShowCheckedModeBanner: false,
          initialRoute: '/',
          routes: {
            '/search_screen': (context) => SearchScreen(),
          },
          theme: ThemeData(
            brightness: Brightness.dark,
          ),
          home: FutureBuilder(
            future: _repository.getCurrentUser(),
            builder: (context, AsyncSnapshot<FirebaseUser> _snapshot) {
              if (_snapshot.hasData) {
                return HomeScreen();
              } else {
                return WillPopScope(
                  onWillPop: () => Future(() => _onBackPressed()),
                  child: LoginScreen(),
                );
              }
            },
          )),
    );
  }

  _onBackPressed() async {
    backPressed++;
    if (backPressed > 1) {
      backPressed = 0;
      await SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop');
    }
  }
}
