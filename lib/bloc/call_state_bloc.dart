import 'dart:async';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:kudos/bloc/call_state_provider.dart';
import 'package:rxdart/rxdart.dart';
import './../enum/call_state.dart';

class CallBloc extends BlocBase {
  CallBloc();

//Stream that receives a number and changes the count;
  var _callStateController =
      BehaviorSubject<CallState>.seeded(CallState.IDLE);

  Stream<CallState> get callStateStream => _callStateController.stream;

  Sink<CallState> get callStateSink => _callStateController.sink;

  picked() {
    callStateSink.add(CallState.PICKED);
  }

  rejected() {
    callStateSink.add(CallState.REJECTED);
  }

  incoming() {
    callStateSink.add(CallState.INCOMING);
  }

  dialled() {
    callStateSink.add(CallState.DIALLED);
  }

  idle() {
    callStateSink.add(CallState.IDLE);
  }

  missed() {
    callStateSink.add(CallState.MISSED);
  }

//dispose will be called automatically by closing its streams
  @override
  void dispose() {
    _callStateController.close();
    super.dispose();
  }
}
