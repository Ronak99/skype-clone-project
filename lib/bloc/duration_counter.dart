import 'package:bloc_pattern/bloc_pattern.dart';

class DurationCounter extends BlocBase {
  int duration = 0;

  incrementDuration() {
    print("inside duration incremement");
    duration = duration + 1;
    notifyListeners();
  }

  int get getDuration => duration;

  resetDuration() {
    duration = 0;
    notifyListeners();
  }
}
