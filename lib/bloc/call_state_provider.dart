import './../enum/call_state.dart';

class CallStateProvider {
  CallState currentCallState = CallState.IDLE;

  picked() => this.currentCallState = CallState.PICKED;
  incoming() => this.currentCallState = CallState.INCOMING;
  rejected() => this.currentCallState = CallState.REJECTED;
  dialled() => this.currentCallState = CallState.DIALLED;
}
