import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import './../enum/call_state.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/screens/callscreens/pickup_screen.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/streamChanger.dart';
import 'package:kudos/utils/utilities.dart';
import 'package:kudos/widgets/custom_tile.dart';
import 'chatscreens/chat_screen.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  //variables from other classes
  FirebaseRepository _repository = FirebaseRepository();

  //dont do stream changer just yet
  StreamChanger changer;

  //variables related to this class
  List<User> userList = List<User>();
  String query = "";
  //User _currentUser = User();
  TextEditingController searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    changer = StreamChanger();

    //and our first step would be to retrieve the details of the current user.

    _repository.getCurrentUser().then((user) {
      // _repository.fetchUserDetailsById(user.uid).then((user) {
      //   setState(() {
      //     _currentUser = user;
      //   });
      // });
      //after this I'll query the entire database.
      _repository.fetchAllUsers(user).then((list) {
        setState(() {
          userList = list;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    CallBloc bloc = UniversalVariables.bloc;

    changer.changeStream();

    return StreamBuilder<CallState>(
      stream: bloc.callStateStream,
      initialData: CallState.IDLE,
      builder: (context, snapshot) {
        Utils.handleRintone(snapshot);

        // return (snapshot.data == CallState.INCOMING)
        //     ? PickupScreen(receiverId: _currentUser.uid)
        //     : buildScaffold();

        return buildScaffold();
      },
    );
  }

  Widget buildScaffold() {
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      appBar: searchAppBar(context),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: buildSuggestions(query),
      ),
    );
  }

  //so we are gonna create a customappbar here as well, but since it is associated to this screen only, therefore we wont be creating it in the form of a widget in a separate file.

  searchAppBar(BuildContext context) {
    //gradient app bar is a built in appbar widget with gradients
    return GradientAppBar(
      backgroundColorStart: UniversalVariables.gradientColorStart,
      backgroundColorEnd: UniversalVariables.gradientColorEnd,
      leading: IconButton(
        icon: Icon(Icons.arrow_back, color: Colors.white),
        onPressed: () => Navigator.pop(context),
      ),
      elevation: 0,
      bottom: PreferredSize(
        preferredSize: const Size.fromHeight(kToolbarHeight + 20),
        child: Padding(
          padding: EdgeInsets.only(left: 20),
          child: TextField(
            controller: searchController,
            onChanged: (val) {
              setState(() {
                query = val;
              });
            },
            cursorColor: UniversalVariables.blackColor,
            autofocus: true,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.white,
              fontSize: 35,
            ),
            decoration: InputDecoration(
              suffixIcon: IconButton(
                icon: Icon(Icons.close, color: Colors.white),
                onPressed: () {
                  WidgetsBinding.instance.addPostFrameCallback( (_) => searchController.clear());
                },
              ),
              border: InputBorder.none,
              hintText: "Search",
              hintStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 35,
                color: Color(0x88ffffff),
              ),
            ),
          ),
        ),
      ),
    );
  }

    buildSuggestions(String query) {
    final List<User> suggestionList = query.isEmpty
        ? []
        : userList.where((User user) {
            String _getUsername = user.username.toLowerCase();
            String _query = query.toLowerCase();
            String _getName = user.name.toLowerCase();
            bool matchesUsername = _getUsername.contains(_query);
            bool matchesName = _getName.contains(_query);

            return (matchesUsername || matchesName);

            // (User user) => (user.username.toLowerCase().contains(query.toLowerCase()) ||
            //     (user.name.toLowerCase().contains(query.toLowerCase()))),
          }).toList();
    return ListView.builder(
      itemCount: suggestionList.length,
      itemBuilder: ((context, index) {
        User searchedUser = User(
            uid: suggestionList[index].uid,
            profilePhoto: suggestionList[index].profilePhoto,
            name: suggestionList[index].name,
            username: suggestionList[index].username);

        return CustomTile(
          mini: false,
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatScreen(
                          receiver: searchedUser,
                        )));
          },
          leading: CircleAvatar(
            backgroundImage: NetworkImage(searchedUser.profilePhoto),
            backgroundColor: Colors.grey,
          ),
          title: Text(
            searchedUser.username,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            searchedUser.name,
            style: TextStyle(color: UniversalVariables.greyColor),
          ),
        );
      }),
    );
  }
}
