import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kudos/models/call.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/resources/sqlite_methods.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/utilities.dart';
import 'package:kudos/widgets/appbar.dart';
import 'package:kudos/widgets/custom_tile.dart';

class LogScreen extends StatefulWidget {
  //variables related to other classes
  @override
  _LogScreenState createState() => _LogScreenState();
}

class _LogScreenState extends State<LogScreen>
    with AutomaticKeepAliveClientMixin<LogScreen> {
  FirebaseRepository _repository = FirebaseRepository();
  SqliteMethods _sqliteMethods;
  Utils _utils = Utils();

  Future<List<Call>> logList;

  String shortName = "";
  String currentUserId = "";

  @override
  void initState() {
    super.initState();
    _sqliteMethods = SqliteMethods();
    _repository.getCurrentUser().then((user) {
      shortName = user.email.substring(0, 2);
      currentUserId = user.uid;
    });
  }

  refreshLogList() {
    setState(() {
      logList = _sqliteMethods.getLogs();
    });
  }

  @override
  Widget build(BuildContext context) {
    try {
      refreshLogList();
    } catch (e) {
      print("error");
    }

    //Text(currentUserId, style: TextStyle(color: Colors.white),
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      appBar: customAppBar(context),
      floatingActionButton: ButtonRow(),
      body: SafeArea(
        child: Container(child: logListContainer()),
      ),
    );
  }

  Widget logListContainer() {
    return FutureBuilder(
      future: logList,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return logListWidget(snapshot.data);
        }
        return Container();
      },
    );
  }

  Center noLogBox() {
    return Center(
        child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        color: UniversalVariables.separatorColor,
        padding: EdgeInsets.symmetric(vertical: 35, horizontal: 25),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "The Log Screen",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.white),
            ),
            SizedBox(height: 25),
            Text(
              "This is where all of your call logs are listed, so you can review them and access them easily",
              textAlign: TextAlign.center,
              style: TextStyle(
                  letterSpacing: 1.2,
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  color: Colors.white),
            ),
          ],
        ),
      ),
    ));
  }

  Widget logListWidget(List<Call> callList) {
    return callList.length != 0
        ? ListView.builder(
            itemCount: callList.length,
            itemBuilder: (context, index) {
              User receiver = User(
                uid: callList[index].receiverPhoto != null
                    ? callList[index].receiverPhoto
                    : "https://via.placeholder.com/300",
                name: callList[index].receiverName,
                profilePhoto: callList[index].receiverPhoto,
                status: callList[index].status,
              );

              return CustomTile(
                margin: (index == callList.length - 1)
                    ? EdgeInsets.only(bottom: 160)
                    : EdgeInsets.all(0),
                onLongPress: () => showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: Text('Delete?'),
                      content: Text('Are you sure?'),
                      actions: <Widget>[
                        FlatButton(
                          color: UniversalVariables.blackColor,
                          textColor: Colors.white,
                          child: Text("Yes"),
                          onPressed: () {
                            SqliteMethods().deleteLog(callList[index].id);
                            refreshLogList();
                            Navigator.maybePop(context);
                          },
                        ),
                        FlatButton(
                          color: UniversalVariables.blackColor,
                          textColor: Colors.white,
                          child: Text("No"),
                          onPressed: () => Navigator.maybePop(context),
                        ),
                      ],
                    );
                   
                  },
                ),
                leading: CircleAvatar(
                    backgroundImage: NetworkImage(receiver.profilePhoto)),
                icon: Utils.getIconFromStatus(receiver.status),
                subtitle: Text(
                  callList[index].time,
                  style: TextStyle(
                      fontSize: 12, color: UniversalVariables.greyColor),
                ),
                title: Text(
                  receiver.name,
                  style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold),
                ),
                trailing: IconButton(
                  padding: EdgeInsets.symmetric(vertical: 15),
                  icon: Icon(Icons.video_call),
                  color: Colors.white,
                  onPressed: () => _utils.commenceVideoCall(
                      context, currentUserId, receiver),
                ),
              );
            })
        : noLogBox();
  }

  CustomAppBar customAppBar(BuildContext context) {
    return CustomAppBar(
      leading: IconButton(
        icon: Icon(
          Icons.notifications,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      title: Text(
        "Calls",
        style: TextStyle(color: Colors.white),
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/search_screen');
          },
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ButtonRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(50),
            gradient: UniversalVariables.fabGradient,
          ),
          child: Icon(
            Icons.dialpad,
            color: Colors.white,
            size: 25,
          ),
          padding: EdgeInsets.all(15),
        ),
        SizedBox(height: 15),
        Container(
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              color: UniversalVariables.blackColor,
              border: Border.all(
                  width: 2, color: UniversalVariables.gradientColorEnd)),
          child: Icon(
            Icons.add_call,
            color: UniversalVariables.gradientColorEnd,
            size: 25,
          ),
          padding: EdgeInsets.all(15),
        )
      ],
    );
  }
}
