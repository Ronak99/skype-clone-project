import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/widgets/appbar.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:kudos/widgets/custom_tile.dart';
import 'package:permission_handler/permission_handler.dart';

class ContactScreen extends StatefulWidget {
  //variables related to other classes
  @override
  _ContactScreenState createState() => _ContactScreenState();
}

class _ContactScreenState extends State<ContactScreen>
    with AutomaticKeepAliveClientMixin<ContactScreen> {
  String shortName = "";
  Iterable<Contact> _contacts;

  @override
  void initState() {
    super.initState();
    refreshContacts();
  }

  refreshContacts() async {
    PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      var contacts = await ContactsService.getContacts(withThumbnails: false);
//      var contacts = await ContactsService.getContactsForPhone("8554964652");
      setState(() {
        _contacts = contacts;
      });
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: "PERMISSION_DENIED",
          message: "Access to location data denied",
          details: null);
    } else if (permissionStatus == PermissionStatus.disabled) {
      throw new PlatformException(
          code: "PERMISSION_DISABLED",
          message: "Location data is not available on device",
          details: null);
    }
  }

  Future<PermissionStatus> _getContactPermission() async {
    PermissionStatus permission = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.contacts);
    if (permission != PermissionStatus.granted &&
        permission != PermissionStatus.disabled) {
      Map<PermissionGroup, PermissionStatus> permissionStatus =
          await PermissionHandler()
              .requestPermissions([PermissionGroup.contacts]);
      return permissionStatus[PermissionGroup.contacts] ??
          PermissionStatus.unknown;
    } else {
      return permission;
    }
  }

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      appBar: CustomAppBar(
        leading: IconButton(
          icon: Icon(
            Icons.notifications,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        title: Text(
          "Contacts",
          overflow: TextOverflow.ellipsis,
          style: TextStyle(color: Colors.white),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.search,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pushNamed(context, '/search_screen');
            },
          ),
          IconButton(
            icon: Icon(
              Icons.more_vert,
              color: Colors.white,
            ),
            onPressed: () {},
          ),
        ],
      ),
      floatingActionButton: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: UniversalVariables.fabGradient,
        ),
        child: Icon(
          Icons.person_add,
          color: Colors.white,
          size: 25,
        ),
        padding: EdgeInsets.all(15),
      ),
      body: SafeArea(
        child: _contacts != null
            ? ListView.builder(
                addAutomaticKeepAlives: true,
                cacheExtent: 500,
                itemCount: _contacts?.length ?? 0,
                itemBuilder: (BuildContext context, int index) {
                  Contact c = _contacts?.elementAt(index);
                  Iterable<Item> _items = c.phones;
                  Iterable<String> phone = _items.map((i) => i.value ?? "");
                  var numberWithCode;
                  var number;
                  try {
                    numberWithCode = phone.first.startsWith("+91")
                        ? phone.first
                        : "+91" + phone.first;

                    number = numberWithCode != null ? numberWithCode : 0;
                  } catch (e) {}

                  bool hideInvite;

                  return FutureBuilder(
                      future: Firestore.instance
                          .collection("users")
                          .where("phone", isEqualTo: number)
                          .getDocuments(),
                      builder:
                          (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (!snapshot.hasData && snapshot.data != null) {}
                        hideInvite = snapshot.data != null
                            ? snapshot.data.documents.contains(number)
                            : true;


                        return CustomTile(
                          margin: (index == _contacts.length-1) ? EdgeInsets.only(bottom: 90) : EdgeInsets.all(0),
                          icon: Container(),
                          leading: (c.avatar != null && c.avatar.length > 0)
                              ? CircleAvatar(
                                  backgroundImage: MemoryImage(c.avatar))
                              : CircleAvatar(child: Text(c.initials())),
                          title: SizedBox(
                            width: 150,
                            child: Text(
                              c.displayName ?? "",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                              ),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          subtitle: Text(
                            "From your address book",
                            style: TextStyle(
                                fontSize: 12,
                                color: UniversalVariables.greyColor),
                          ),
                          trailing: hideInvite != null
                              ? trailingWidget(hideInvite)
                              : SizedBox(height: 55),
                        );
                      });
                },
              )
            : Center(
                child: CircularProgressIndicator(),
              ),
      ),
    );
  }

  trailingWidget(hideInvite) {
    return hideInvite ? SizedBox(height: 55) : InviteButton();
  }
}

class InviteButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: BoxDecoration(
          border: Border.all(width: 1, color: Colors.white),
          borderRadius: BorderRadius.circular(25)),
      child: Text(
        "Invite",
        style: TextStyle(fontSize: 12, color: Colors.white),
      ),
    );
  }
}
