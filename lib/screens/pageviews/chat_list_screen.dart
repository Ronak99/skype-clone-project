import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:kudos/enum/user_state.dart';
import 'package:kudos/main.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/screens/chatscreens/chat_screen.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/utilities.dart';
import 'package:kudos/widgets/appbar.dart';
import 'package:kudos/widgets/custom_tile.dart';
import 'package:shimmer/shimmer.dart';

class ChatListScreen extends StatefulWidget {
  //variables related to other classes
  @override
  _ChatListScreenState createState() => _ChatListScreenState();
}

final FirebaseRepository _repository = FirebaseRepository();

class _ChatListScreenState extends State<ChatListScreen>
    with AutomaticKeepAliveClientMixin<ChatListScreen> {
  String shortName = "";
  String currentUserId;

  @override
  void initState() {
    super.initState();
    _repository.getCurrentUser().then((user) {
      setState(() {
        shortName = Utils.getInitials(user.displayName);
        currentUserId = user.uid;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      appBar: customAppBar(context),
      floatingActionButton: NewChatButton(),
      body: ChatListContainer(currentUserId),
    );
  }

  CustomAppBar customAppBar(BuildContext context) {
    return CustomAppBar(
      leading: IconButton(
        icon: Icon(
          Icons.notifications,
          color: Colors.white,
        ),
        onPressed: () {},
      ),
      title: GestureDetector(
        child: UserCircle(shortName),
        onTap: () => showModalBottomSheet(
          isScrollControlled: true,
          context: context,
          backgroundColor: UniversalVariables.blackColor,
          builder: (context) => UserDetails(currentUserId: currentUserId),
        ),
      ),
      centerTitle: true,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pushNamed(context, '/search_screen');
          },
        ),
        IconButton(
          icon: Icon(
            Icons.more_vert,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class ShimmerImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      width: 100,
      child: Shimmer.fromColors(
        baseColor: UniversalVariables.blackColor,
        highlightColor: Colors.white,
        child: Image.asset("thecsguy.png"),
        period: Duration(seconds: 2),
      ),
    );
  }
}

class UserDetails extends StatelessWidget { 
  UserDetails({@required this.currentUserId});

  final String currentUserId;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 25),
      child: Column(
        children: <Widget>[
          CustomAppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
              onPressed: () => Navigator.maybePop(context),
            ),
            centerTitle: true,
            title: ShimmerImage(),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  print("sign out pressed");
                  _repository.signOut().then((_) {
                    print("inside then callback");
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => MyApp()));
                  });
                },
                child: Text(
                  "Sign Out",
                  style: TextStyle(color: Colors.white, fontSize: 12),
                ),
              )
            ],
          ),
          Container(
              child: FutureBuilder(
            future: Firestore.instance
                .collection("users")
                .document(currentUserId)
                .get(),
            builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }

              return snapshot != null ? detailsBox(snapshot) : Container();
            },
          )),
        ],
      ),
    );
  }

  detailsBox(snapshot) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Row(
        children: [
          CircleAvatar(
            radius: 40,
            backgroundImage: NetworkImage(snapshot.data["profile_photo"]),
          ),
          SizedBox(width: 15),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                snapshot.data["name"],
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: 10),
              Text(
                snapshot.data["email"],
                style: TextStyle(fontSize: 14, color: Colors.white),
              ),
            ],
          ),
        ],
      ),
    );
  }
}


class UserCircle extends StatelessWidget {
  final String text;

  UserCircle(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: 40,
      decoration: BoxDecoration(
        color: UniversalVariables.separatorColor,
        shape: BoxShape.circle,
      ),
      child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Text(
                text,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: UniversalVariables.lightBlueColor,
                    fontSize: 13),
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                height: 12,
                width: 12,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border:
                        Border.all(color: UniversalVariables.blackColor, width: 2),
                    color: UniversalVariables.onlineDotColor),
              ),
            )
          ],
        ),
    );
  }
}

class NewChatButton extends StatelessWidget {
  const NewChatButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(50),
          gradient: UniversalVariables.fabGradient,
          ),
      child: Icon(
        Icons.edit,
        color: Colors.white,
        size: 25,
      ),
      padding: EdgeInsets.all(15),
    );
  }
}

class ChatListContainer extends StatefulWidget {
  final String currentUserId;

  ChatListContainer(this.currentUserId);

  @override
  ChatListContainerState createState() => ChatListContainerState();
}

class ChatListContainerState extends State<ChatListContainer> {
  @override
  Widget build(BuildContext context) {
    if (widget.currentUserId != "") {
      return Container(
          child: StreamBuilder(
        stream: Firestore.instance
            .collection("users")
            .document(widget.currentUserId)
            .collection("contacts")
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.data == null) {
            return Center(child: CircularProgressIndicator());
          }
          return snapshot.data.documents.isEmpty
              ? quiteBox()
              : listview(snapshot);
        },
      ));
    }
    return Container();
  }

  ListView listview(AsyncSnapshot<QuerySnapshot> snapshot) {
    return ListView.builder(
        padding: EdgeInsets.all(10.0),
        itemCount: snapshot.data.documents.length,
        itemBuilder: (context, index) {
          DocumentSnapshot doc = snapshot.data.documents[index];

          User receiver = User(
              uid: doc.data["contact_id"],
              profilePhoto: doc.data["contact_photo"],
              name: doc.data["contact_name"]);

          return CustomTile(
            margin: (index == snapshot.data.documents.length - 1)
                ? EdgeInsets.only(bottom: 90)
                : EdgeInsets.all(0),
            mini: false,
            onTap: () => Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ChatScreen(receiver: receiver))),
            title: Text(
              receiver.name,
              style: TextStyle(
                  color: Colors.white, fontFamily: "Arial", fontSize: 19),
            ),
            leading: Container(
              constraints: BoxConstraints(maxHeight: 60, maxWidth: 60),
              child: Stack(
                children: <Widget>[
                  CircleAvatar(
                    maxRadius: 30,
                    backgroundImage: NetworkImage(doc.data["contact_photo"]),
                    backgroundColor: Colors.grey,
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: StreamBuilder(
                      stream: Firestore.instance
                          .collection("users")
                          .document(doc.data["contact_id"])
                          .snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return Container(
                            height: 13,
                            width: 13,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              border: Border.all(
                                  color: UniversalVariables.blackColor,
                                  width: 2),
                              color: getDotColorFromUserState(
                                  Utils.numToState(snapshot.data["state"])),
                            ),
                          );
                        }
                        return Container(
                          height: 13,
                          width: 13,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(
                                color: UniversalVariables.blackColor, width: 2),
                            color: UniversalVariables.onlineDotColor,
                          ),
                        );
                      },
                    ),
                  )
                ],
              ),
            ),
            subtitle: LastMessageContainer(
                widget.currentUserId, doc.data["contact_id"]),
          );
        });
  }

  Color getDotColorFromUserState(UserState userState) {
    switch (userState) {
      case UserState.Online:
        return UniversalVariables.onlineDotColor;
        break;

      case UserState.Offline:
        return Colors.redAccent;
        break;

      default:
        return Colors.yellow[900];
        break;
    }
  }

  Center quiteBox() {
    return Center(
        child: Padding(
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Container(
        color: UniversalVariables.separatorColor,
        padding: EdgeInsets.symmetric(vertical: 35, horizontal: 25),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              "It's very quite here,",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Colors.white),
            ),
            SizedBox(height: 25),
            Text(
              "Search for your friends and family to start calling or chatting with them",
              textAlign: TextAlign.center,
              style: TextStyle(
                  letterSpacing: 1.2,
                  fontWeight: FontWeight.normal,
                  fontSize: 18,
                  color: Colors.white),
            ),
          ],
        ),
      ),
    ));
  }
}

class LastMessageContainer extends StatelessWidget {
  final String currentUserId;
  final String otherUserId;

  LastMessageContainer(this.currentUserId, this.otherUserId);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: Firestore.instance
          .collection("messages")
          .document(currentUserId)
          .collection(otherUserId)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            break;

          default:
            try {
              return SizedBox(
                width: MediaQuery.of(context).size.width * 0.6,
                child: Text(
                  snapshot.data.documents.last.data["message"],
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                      color: UniversalVariables.greyColor, fontSize: 14),
                ),
              );
            } catch (e) {
              return Text(
                "No Message",
                style: TextStyle(
                    color: UniversalVariables.greyColor, fontSize: 14),
              );
            }
        }
        return Container();
      },
    );
  }
}
