import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './../enum/call_state.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/enum/user_state.dart';

import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/resources/sqlite_methods.dart';
import 'package:kudos/screens/pageviews/contacts_screen.dart';
import 'package:kudos/screens/callscreens/pickup_screen.dart';

import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/streamChanger.dart';
import 'package:kudos/utils/utilities.dart';
import 'pageviews/chat_list_screen.dart';
import 'pageviews/log_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  //reposoitory
  FirebaseRepository _repository = FirebaseRepository();
  //page view related variables
  String currentUserId;
  PageController pageController;
  int _page = 0;
  bool isCalling = false;
  String channelId;
  String callBy;

  var _sqliteMethods;

  StreamChanger changer;

  @override
  void initState() {
    super.initState();
    _sqliteMethods = SqliteMethods();
    WidgetsBinding.instance.addObserver(this);
    pageController = new PageController();
    changer = StreamChanger();
    getUserId();
  }

  void navigationTapped(int page) {
    //Animating Page
    pageController.jumpToPage(page);
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  void dispose() {
    super.dispose();
    print("screen disposed");
    WidgetsBinding.instance.removeObserver(this);
    pageController.dispose();
  }

  Future<void> getUserId() async {
    await _repository.getCurrentUser().then((user) {
      currentUserId = user.uid;
      _repository.setUserState(currentUserId, UserState.Online);
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.paused:
        currentUserId != null
            ? _repository.setUserState(currentUserId, UserState.Waiting)
            : print("paused state");
        break;
      case AppLifecycleState.resumed:
        currentUserId != null
            ? _repository.setUserState(currentUserId, UserState.Online)
            : print("paused state");

        print("resumed");
        break;
      case AppLifecycleState.inactive:
        currentUserId != null
            ? _repository.setUserState(currentUserId, UserState.Offline)
            : print("paused state");

        print('inactive state');
        break;
      case AppLifecycleState.suspending:
        currentUserId != null
            ? _repository.setUserState(currentUserId, UserState.Offline)
            : print("paused state");

        print('suspending state');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    CallBloc bloc = UniversalVariables.bloc;

    changer.changeStream();

    return  StreamBuilder<CallState>(
        stream: bloc.callStateStream,
        initialData: CallState.IDLE,
        builder: (context, snapshot) {
          Utils.handleRintone(snapshot);

          return (snapshot.data == CallState.INCOMING)
              ? PickupScreen(receiverId: currentUserId)
              : buildScaffold();
        },
      );
  }

  Widget buildScaffold() {
    double _labelFontSize = 10;
    return Scaffold(
        backgroundColor: UniversalVariables.blackColor,
        body: PageView(
          children: [
            Container(child: ChatListScreen()),
            Container(child: LogScreen()),
            Container(child: ContactScreen()),
          ],
          controller: pageController,

          ///show people that trick, where you uncomment the physics property
          ///when the code is running
          physics: NeverScrollableScrollPhysics(),
          onPageChanged: onPageChanged,
        ),
        bottomNavigationBar: Container(
          decoration: BoxDecoration(
            border: Border(
              top: BorderSide(width: 2, color: UniversalVariables.senderColor)
            )
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 10),
            child: CupertinoTabBar(
              border: Border.all(color: UniversalVariables.blackColor),
              backgroundColor: UniversalVariables.blackColor,
              //then I am gonna pass it a list of items
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  ///and it takes an icon and a title,
                  ///so we want to actually change the color of the icon
                  ///and the text as the user clicks away from the 
                  ///selected tab
                  icon: Icon(Icons.chat,
                  
                      color: (_page == 0)
                          ? UniversalVariables.lightBlueColor
                          : UniversalVariables.greyColor),
                          ///so if we are on the 0th page, then we want
                          ///the icon to be lightblue in color or else 
                          ///we want it to be grey in color
                  title: Text("Chats",
                      style: TextStyle(
                          fontSize: _labelFontSize,
                          color: (_page == 0)
                              ? UniversalVariables.lightBlueColor
                              : Colors.grey)),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.call,
                      color: (_page == 1)
                          ? UniversalVariables.lightBlueColor
                          : Colors.grey),
                  title: Text("Calls",
                      style: TextStyle(
                          fontSize: _labelFontSize,
                          color: (_page == 1)
                              ? UniversalVariables.lightBlueColor
                              : Colors.grey)),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.contact_phone,
                      color: (_page == 2)
                          ? UniversalVariables.lightBlueColor
                          : Colors.grey),
                  title: Text("Contacts",
                      style: TextStyle(
                          fontSize: _labelFontSize,
                          color: (_page == 2)
                              ? UniversalVariables.lightBlueColor
                              : Colors.grey)),
                ),
              ],
              onTap: navigationTapped,
              currentIndex: _page,
            ),
          ),
        ));
  }
}
