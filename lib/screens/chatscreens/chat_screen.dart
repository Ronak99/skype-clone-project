import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:keyboard_visibility/keyboard_visibility.dart';
import 'package:kudos/enum/call_state.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/models/message.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/resources/sqlite_methods.dart';
import 'package:kudos/screens/callscreens/pickup_screen.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/streamChanger.dart';
import 'package:kudos/utils/utilities.dart';
import 'package:kudos/widgets/appbar.dart';
import 'package:emoji_picker/emoji_picker.dart';
import 'package:kudos/widgets/custom_tile.dart';
import 'package:image/image.dart' as Im;
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:kudos/screens/chatscreens/image_viewer_screen.dart';

class ChatScreen extends StatefulWidget {
  final User receiver;

  ChatScreen({@required this.receiver});

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  //repository
  FirebaseRepository _repository = FirebaseRepository();
  SqliteMethods _sqliteMethods;

  Utils _utils = Utils();

  String _currentUserId;
  File imageFile;
  ScrollController _listScrollController = ScrollController();
  StreamChanger changer;

  User sender;

  bool isWriting = false;
  bool showEmojiPicker = false;
  TextEditingController textFieldController = TextEditingController();

  int drawSender = 0;
  int drawReceiver = 0;
  @protected
  void initState() {
    _sqliteMethods = SqliteMethods();
    changer = StreamChanger();
    super.initState();

    KeyboardVisibilityNotification().addNewListener(
      onChange: (bool visible) {
        if (visible) {
          setState(() {
            showEmojiPicker = false;
          });
        }
      },
    );

    _repository.getCurrentUser().then((user) {
      _currentUserId = user.uid;

      _repository.fetchUserDetailsById(_currentUserId).then((user) {
        setState(() {
          sender = User(
              uid: user.uid, name: user.name, profilePhoto: user.profilePhoto);
        });
      });
      /*_repository.fetchUserDetailsById(widget.receiverId).then((user) {
        setState(() {
          receiverPhotoUrl = user.profilePhoto;
          receiverName = user.name;
        });
      });*/
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    CallBloc bloc = UniversalVariables.bloc;

    changer.changeStream();

    return StreamBuilder<CallState>(
      stream: bloc.callStateStream,
      initialData: CallState.IDLE,
      builder: (context, snapshot) {
        Utils.handleRintone(snapshot);

        return (snapshot.data == CallState.INCOMING)
            ? PickupScreen(receiverId: _currentUserId)
            : buildScaffold();
      },
    );
  }

  buildScaffold() {
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      appBar: customAppBar(context),
      body: Column(
        children: <Widget>[
          Flexible(
            child: messageList(),

            ///because messageList would definitely contain a list, and if you want to place a list inside of a column then you are gonna need to wrap the listview with either flexible widget or an expandable widget which extends flexible.
          ),
          chatControls(),
          showEmojiPicker ? Container(child: emojiKeyboard()) : Container()
        ],
      ),
    );
  }

  emojiKeyboard() {
    return EmojiPicker(
      bgColor: UniversalVariables.separatorColor,
      indicatorColor: UniversalVariables.blueColor,
      rows: 3,
      columns: 7,
      recommendKeywords: ["racing", "horse"],
      numRecommended: 10,
      onEmojiSelected: (emoji, category) {
        textFieldController.text = textFieldController.text + emoji.emoji;
      },
    );
  }

  invitationBox() {
    return Padding(
      padding: EdgeInsets.all(30),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 40),
        width: double.infinity,
        color: UniversalVariables.receiverColor,
        alignment: Alignment.center,
        child: Text(
          "Waiting for ${widget.receiver.name} to accept your invitation",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget chatControls() {
    setWritingTo(bool val) {
      setState(() {
        isWriting = val;
        print("set state is being called everytime the user typed");
      });
    }

    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                gradient: UniversalVariables.fabGradient,
                borderRadius: BorderRadius.circular(20)),
            child: GestureDetector(
              onTap: () => _addMediaModal(context),
              child: Icon(Icons.add),
            ),
          ),
          SizedBox(width: 5),
          Expanded(
            child: TextField(
              style: TextStyle(color: Colors.white),
              controller: textFieldController,
              onChanged: (val) {
                (val.length > 0 && val.trim() != "")
                    ? setWritingTo(true)
                    : setWritingTo(false);
              },
              decoration: InputDecoration(
                suffixIcon: GestureDetector(
                  onTap: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    setState(() {
                      showEmojiPicker = !showEmojiPicker;
                    });
                  },
                  child: Icon(Icons.face),
                ),
                hintText: "Type a message",
                hintStyle: TextStyle(
                  color: UniversalVariables.greyColor,
                ),
                border: OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(50.0),
                    ),
                    borderSide: BorderSide.none),
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                filled: true,
                fillColor: UniversalVariables.separatorColor,
              ),
            ),
          ),
          isWriting
              ? Container()
              : Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Icon(Icons.record_voice_over),
                ),
          isWriting
              ? Container()
              : GestureDetector(
                  child: Icon(Icons.camera_alt),
                  onTap: () => pickImage(source: ImageSource.camera)),
          isWriting
              ? Container(
                  margin: EdgeInsets.only(left: 10),
                  decoration: BoxDecoration(
                      gradient: UniversalVariables.fabGradient,
                      shape: BoxShape.circle),
                  child: IconButton(
                    icon: Icon(
                      Icons.send,
                      size: 15,
                    ),
                    onPressed: () => sendMessage(),
                  ))
              : Container()
        ],
      ),
    );
  }

  Future<void> pickImage({@required ImageSource source}) async {
    var selectedImage = await ImagePicker.pickImage(source: source);

    setState(() {
      imageFile = selectedImage;
    });
    compressImage();

    _repository.showLoading(widget.receiver.uid, _currentUserId);

    _repository.uploadImageToStorage(imageFile).then((url) {
      print("URL: $url");
      _repository.hideLoading(widget.receiver.uid, _currentUserId);
      _repository.uploadImageMsgToDb(url, widget.receiver.uid, _currentUserId);
    });
    return;
  }

  void compressImage() async {
    print('starting compression');
    final tempDir = await getTemporaryDirectory();
    final path = tempDir.path;
    int rand = Random().nextInt(10000);

    Im.Image image = Im.decodeImage(imageFile.readAsBytesSync());
    Im.copyResize(image, width: 500, height: 500);

    var newim2 = new File('$path/img_$rand.jpg')
      ..writeAsBytesSync(Im.encodeJpg(image, quality: 85));

    setState(() {
      imageFile = newim2;
    });
    print('done');
  }

  _addMediaModal(context) {
    showModalBottomSheet(
        context: context,
        elevation: 0,
        backgroundColor: UniversalVariables.blackColor,
        builder: (BuildContext bc) {
          return Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: Row(
                  children: <Widget>[
                    FlatButton(
                      child: Icon(
                        Icons.close,
                      ),
                      onPressed: () => Navigator.maybePop(context),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Content and tools",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Flexible(
                child: ListView(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () {
                        pickImage(source: ImageSource.gallery);
                        Navigator.maybePop(context);
                      },
                      child: ModalTile(
                        title: "Media",
                        subtitle: "Share photos and videos",
                        icon: Icons.image,
                      ),
                    ),
                    ModalTile(
                        title: "File",
                        subtitle: "Share files",
                        icon: Icons.tab),
                    ModalTile(
                        title: "Contact",
                        subtitle: "Share contacts",
                        icon: Icons.contacts),
                    ModalTile(
                        title: "Location",
                        subtitle: "Share a location",
                        icon: Icons.add_location),
                    ModalTile(
                        title: "Schedule Call",
                        subtitle: "Arrange a skype call and get reminders",
                        icon: Icons.schedule),
                    ModalTile(
                        title: "Create Poll",
                        subtitle: "Share polls",
                        icon: Icons.poll)
                  ],
                ),
              )
            ],
          );
        });
  }

  Widget messageList() {
    return StreamBuilder(
      stream: Firestore.instance
          .collection('messages')
          .document(_currentUserId)
          .collection(widget.receiver.uid)
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          //listItem = snapshot.data.documents;

          SchedulerBinding.instance.addPostFrameCallback((_) {
            _listScrollController.animateTo(
              //_listScrollController.position.maxScrollExtent,
              0,
              duration: const Duration(milliseconds: 250),
              curve: Curves.easeOut,
            );
          });

          return ListView.builder(
            reverse: true,
            controller: _listScrollController,
            padding: EdgeInsets.all(10.0),
            itemCount: snapshot.data.documents.length,
            itemBuilder: (context, index) =>
                chatMessageItem(snapshot.data.documents[index]),
          );
        }
      },
    );
  }

  Widget chatMessageItem(DocumentSnapshot snapshot) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15),
      child: snapshot['type'] == 'special'
          ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(snapshot['message'],
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white)),
                ),
              ],
            )
          : Container(
              alignment: snapshot['senderId'] == _currentUserId
                  ? Alignment.centerRight
                  : Alignment.centerLeft,
              child: snapshot['senderId'] == _currentUserId
                  ? senderLayout(snapshot)
                  : receiverLayout(snapshot),
            ),
    );
  }

  Widget senderLayout(DocumentSnapshot snapshot) {
    Radius messageRadius = Radius.circular(10);
    drawReceiver = 0;
    drawSender = drawSender + 1;

    //returning the text containing container
    return snapshot['type'] != 'loading'
        ? Container(
            margin: EdgeInsets.only(top: drawSender > 1 ? 1 : 12),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.65),
            decoration: BoxDecoration(
              color: UniversalVariables.senderColor,
              borderRadius: BorderRadius.only(
                topLeft: messageRadius,
                topRight: messageRadius,
                bottomLeft: messageRadius,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: getMessage(snapshot),
            ),
          )
        : CircularProgressIndicator();
  }

  Widget getMessage(snapshot) {
    if (snapshot['type'] == 'image') {
      final String heroTag = Utils.generateRandomString();
      return GestureDetector(
        onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => ImageViewer(
                      heroTag: heroTag,
                      photoUrl: snapshot["photoUrl"],
                    ))),
        child: Hero(
          tag: heroTag,
          child: FadeInImage(
            fit: BoxFit.cover,
            image: NetworkImage(snapshot['photoUrl']),
            placeholder: AssetImage('assets/blankimage.png'),
            width: 200.0,
            height: 200.0,
          ),
        ),
      );
    }

    return Text(snapshot['message'],
        style: TextStyle(
          color: Colors.white,
          fontSize: 16.0,
        ));
  }

  Widget receiverLayout(DocumentSnapshot snapshot) {
    Radius messageRadius = Radius.circular(10);
    drawSender = 0;
    drawReceiver = drawReceiver + 1;

    return Container(
      margin: EdgeInsets.only(top: drawReceiver > 1 ? 1 : 12),
      constraints:
          BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.65),
      decoration: BoxDecoration(
        color: UniversalVariables.receiverColor,
        borderRadius: BorderRadius.only(
          bottomRight: messageRadius,
          topRight: messageRadius,
          bottomLeft: messageRadius,
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: getMessage(snapshot),
      ),
    );
  }

  void sendMessage() {
    var text = textFieldController.text;

    Message _message = Message(
        receiverId: widget.receiver.uid,
        senderId: sender.uid,
        message: text,
        timestamp: FieldValue.serverTimestamp(),
        type: 'text');
    textFieldController.text = "";
    setState(() {
      isWriting = false;
    });

    _repository.addMessageToDb(_message, sender, widget.receiver).then((v) {});
  }

  CustomAppBar customAppBar(BuildContext context) {
    return CustomAppBar(
      leading: IconButton(
        icon: Icon(
          Icons.arrow_back,
        ),
        //here we don't need to specify any icon color, because these icons and texts are ultimately going to an appbar widget, which by default renders its child as white in colour, infact, since all the icons are going to be white, therefore, I am just gonna go over to main.dart, and define a theme, which accepts themedata. And, set the brightness to dark. So now, you would not have to define the icon colour anywhere, except, if you are using an icon inside of a textfield. Alright.
        onPressed: () {
          Navigator.pop(context);
        },
      ),
      //now, we dont want the title to be in center,
      centerTitle: false,
      title: Text(
        widget.receiver.name,
        //this is gonna be the only dynamic portion of this screen.
      ),
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.video_call,
          ),
          onPressed: () => _utils.commenceVideoCall(
              context, _currentUserId, widget.receiver),
        ),
        IconButton(
          icon: Icon(
            Icons.phone,
          ),
          onPressed: () => _sqliteMethods.addLogs(widget.receiver, "MISSED"),
        )
      ],
    );
  }
}

class ModalTile extends StatelessWidget {
  const ModalTile(
      {Key key,
      @required this.title,
      @required this.subtitle,
      @required this.icon})
      : super(key: key);

  final String title;
  final String subtitle;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15),
      //so this customtile is also one of the custom widgets, present inside of the widgets folder.
      child: CustomTile(
        mini: false,
        leading: Container(
          margin: EdgeInsets.only(right: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
            color: UniversalVariables.receiverColor,
          ),
          padding: EdgeInsets.all(10),
          child: Icon(icon, color: UniversalVariables.greyColor, size: 38),
        ),
        subtitle: Text(
          subtitle,
          style: TextStyle(
            color: UniversalVariables.greyColor,
            fontSize: 14,
          ),
        ),
        title: Text(
          title,
          style: TextStyle(
              fontWeight: FontWeight.bold, color: Colors.white, fontSize: 18),
        ),
      ),
    );
  }
}
