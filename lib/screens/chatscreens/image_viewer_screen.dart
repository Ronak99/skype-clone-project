import 'package:flutter/material.dart';
import 'package:kudos/theme/universal_colors.dart';

class ImageViewer extends StatefulWidget {
  final String photoUrl;
  final String heroTag;

  ImageViewer({this.photoUrl, this.heroTag});

  @override
  _ImageViewerState createState() => _ImageViewerState();
}

class _ImageViewerState extends State<ImageViewer> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      body: SafeArea(
        child: Hero(
          tag: widget.heroTag,
          child: FadeInImage(
            fit: BoxFit.contain,
            image: NetworkImage(widget.photoUrl),
            placeholder: AssetImage('assets/blankimage.png'),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
        ),
      ),
    );
  }
}
