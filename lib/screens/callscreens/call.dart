import 'dart:async';
import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:bloc_pattern/bloc_pattern.dart';
import 'package:flutter/material.dart';
import 'package:kudos/bloc/duration_counter.dart';
import 'package:kudos/enum/call_state.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/models/message.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/resources/sqlite_methods.dart';
import 'package:kudos/utils/utilities.dart';
import 'package:kudos/models/videosession.dart';
import 'package:kudos/utils/settings.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class CallPage extends StatefulWidget {
  /// non-modifiable channel name of the page
  final String channelName;
  final bool calling;
  final String callerId;
  final String receiverId;
  final String contactName;
  final String contactPhoto;

  /// Creates a call page with given channel name.
  const CallPage({
    Key key,
    @required this.channelName,
    @required this.calling,
    @required this.callerId,
    @required this.receiverId,
    @required this.contactName,
    @required this.contactPhoto,
  }) : super(key: key);

  @override
  _CallPageState createState() {
    return new _CallPageState();
  }
}

class _CallPageState extends State<CallPage> {
  //repository
  FirebaseRepository _repository = FirebaseRepository();
  Firestore db = Firestore.instance;
  SqliteMethods _sqliteMethods = SqliteMethods();

  static final _sessions = List<VideoSession>();
  final _infoStrings = <String>[];
  bool muted = false;

  bool localCalling;

  int callDuration = 0;

  Timer _timer;
  Timer _anotherTimer;

  bool insideRejected = false;

  final String callToneUrl = "https://mobcup.net/d/o3drz01f/mp3";

  final CallBloc bloc = BlocProvider.getBloc<CallBloc>();
  DurationCounter durationCounter = BlocProvider.getBloc<DurationCounter>();
  AudioPlayer audioPlayer;

  @override
  void dispose() {
    // clean up native views & destroy sdk
    _sessions.forEach((session) {
      AgoraRtcEngine.removeNativeView(session.viewId);
    });
    _sessions.clear();
    AgoraRtcEngine.leaveChannel();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    // initialize agora sdk
    initialize();
    audioPlayer = AudioPlayer();
  }

  void initialize() {
    if (APP_ID.isEmpty) {
      setState(() {
        _infoStrings
            .add("APP_ID missing, please provide your APP_ID in settings.dart");
        _infoStrings.add("Agora Engine is not starting");
      });
      return;
    }

    localCalling = widget.calling;

    _initAgoraRtcEngine();
    _addAgoraEventHandlers();
    // use _addRenderView everytime a native video view is needed
    _addRenderView(0, (viewId) async {
      AgoraRtcEngine.setupLocalVideo(viewId, VideoRenderMode.Hidden);
      AgoraRtcEngine.startPreview();
      // state can access widget directly
      await AgoraRtcEngine.joinChannel(null, widget.channelName, null, 0);
    });
  }

  /// Create agora sdk instance and initialze
  Future<void> _initAgoraRtcEngine() async {
    AgoraRtcEngine.create(APP_ID);
    AgoraRtcEngine.enableVideo();
  }

  /// Add agora event handlers
  void _addAgoraEventHandlers() {
    AgoraRtcEngine.onUserJoined = (int uid, int elapsed) {
      startTimer();
      bloc.picked();
      setState(() {
        String info = 'userJoined: ' + uid.toString();
        _infoStrings.add("elapsed " + elapsed.toString());
        _addRenderView(uid, (viewId) {
          AgoraRtcEngine.setupRemoteVideo(viewId, VideoRenderMode.Hidden, uid);
        });
      });
    };

    AgoraRtcEngine.onLeaveChannel = () {
      //when the other user also leaves the channel
    };

    AgoraRtcEngine.onUserOffline = (int uid, int reason) {
      setState(() {
        String info = 'userOffline: ' + uid.toString();
        _infoStrings.add(info);
        _removeRenderView(uid);
      });
    };

    AgoraRtcEngine.onFirstRemoteVideoFrame =
        (int uid, int width, int height, int elapsed) {
      setState(() {
        String info = 'firstRemoteVideo: ' +
            uid.toString() +
            ' ' +
            width.toString() +
            'x' +
            height.toString();
        _infoStrings.add(info);
      });
    };
  }

  /// Create a native view and add a new video session object
  /// The native viewId can be used to set up local/remote view
  void _addRenderView(int uid, Function(int viewId) finished) {
    Widget view = AgoraRtcEngine.createNativeView((viewId) {
      setState(() {
        _getVideoSession(uid).viewId = viewId;
        if (finished != null) {
          finished(viewId);
        }
      });
    });
    VideoSession session = VideoSession(uid, view);
    _sessions.add(session);
  }

  /// Remove a native view and remove an existing video session object
  void _removeRenderView(int uid) {
    VideoSession session = _getVideoSession(uid);
    if (session != null) {
      _sessions.remove(session);
    }
    AgoraRtcEngine.removeNativeView(session.viewId);
  }

  /// Helper function to filter video session with uid
  VideoSession _getVideoSession(int uid) {
    return _sessions.firstWhere((session) {
      return session.uid == uid;
    });
  }

  /// Helper function to get list of native views
  List<Widget> _getRenderViews() {
    return _sessions.map((session) => session.view).toList();
  }

  /// Video view wrapper
  Widget _videoView(view) {
    return Expanded(
      child: Container(
        child: view,
      ),
    );
  }

  /// Video view row wrapper
  /*Widget _expandedVideoRow(List<Widget> views) {
    List<Widget> wrappedViews =
        views.map((Widget view) => _videoView(view)).toList();
    return Expanded(
        child: Row(
      children: wrappedViews,
    ));
  }*/

  Widget _expandedVideoRow(Widget views) {
    return Positioned(
      top: 20,
      right: 20,
      child: Container(
        height: 200,
        width: 130,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(15)),
        child: views,
      ),
    );
  }

  void startTimer() {
    _timer = Timer.periodic(Duration(seconds: 1), (timer) {
      _anotherTimer = timer;
      durationCounter.incrementDuration();
      callDuration = callDuration + 1;
    });
  }

  void stopTimer() {
    if (_anotherTimer != null && _timer != null) {
      _anotherTimer.cancel();
      _timer.cancel();
      durationCounter.resetDuration();
    }
  }

  /// Video layout wrapper
  Widget _viewRows() {
    List<Widget> views = _getRenderViews();

    if (views.length > 1) {
      bloc.picked();
    } else {}

    switch (views.length) {
      case 1:
        return Container(
            child: Column(
          children: <Widget>[_videoView(views[0])],
        ));
      case 2:
        return Container(
            child: Stack(
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: views[0],
            ),
            Positioned(
              top: 20,
              right: 20,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    height: 200,
                    width: 130,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(15)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: views[1],
                    ),
                  ),
                  SizedBox(height: 10),
                  Consumer<DurationCounter>(
                    builder: (BuildContext context,
                        DurationCounter durationCounter) {
                      return Text(
                        Utils.getTimeFromNumber(durationCounter.getDuration),
                        style: TextStyle(
                          color: Colors.white,
                          shadows: [
                            BoxShadow(color: Colors.black, blurRadius: 12)
                          ],
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ],
        ));
      case 3:
        return Container(
            child: Column(
          children: <Widget>[
            // _expandedVideoRow(views.sublist(0, 2)),
            // _expandedVideoRow(views.sublist(2, 3))
          ],
        ));
      case 4:
        return Container(
            child: Column(
          children: <Widget>[
            // _expandedVideoRow(views.sublist(0, 2)),
            // _expandedVideoRow(views.sublist(2, 4))
          ],
        ));
      default:
    }
    return Container();
  }

  /// Toolbar layout
  Widget _toolbar() {
    double _iconsize = 20.0;

    return Container(
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.symmetric(vertical: 48),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            onPressed: () => _onToggleMute(),
            child: new Icon(
              muted ? Icons.mic : Icons.mic_off,
              color: muted ? Colors.white : Colors.black,
              size: _iconsize,
            ),
            shape: new CircleBorder(),
            elevation: 0,
            fillColor: muted ? Colors.black : Colors.white,
            padding: const EdgeInsets.all(15.0),
          ),
          RawMaterialButton(
            onPressed: () => _onSwitchCamera(),
            child: new Icon(
              Icons.switch_camera,
              color: Colors.black,
              size: _iconsize,
            ),
            elevation: 0,
            shape: new CircleBorder(),
            fillColor: Colors.white,
            padding: const EdgeInsets.all(15.0),
          ),
          RawMaterialButton(
            onPressed: () => _onCallEnd(context),
            child: new Icon(
              Icons.call_end,
              color: Colors.white,
              size: _iconsize,
            ),
            elevation: 0,
            shape: new CircleBorder(),
            fillColor: Colors.red[700],
            padding: const EdgeInsets.all(15.0),
          ),
        ],
      ),
    );
  }

  Widget _onScreenText() {
    return Container(
      child: Align(
        alignment: Alignment.center,
        child: StreamBuilder(
          stream: bloc.callStateStream,
          builder: (context, AsyncSnapshot<CallState> snapshot) {
            switch (snapshot.data) {
              case CallState.DIALLED:
                audioPlayer.play(callToneUrl);
                return Text("Calling ...",
                    style: TextStyle(color: Colors.white));
                break;

              case CallState.REJECTED:
                audioPlayer.stop();
                stopTimer();
                if (callDuration > 0) {
                  String time = Utils.getTimeFromNumber(callDuration);

                  Message _message = Message(
                      receiverId: widget.receiverId,
                      senderId: widget.callerId,
                      message: "Call Ended $time",
                      timestamp: FieldValue.serverTimestamp(),
                      type: 'special');

                  _repository.addCallDetailsToDb(_message, widget.receiverId,
                      widget.callerId, widget.calling);

                  User contact = User(
                      uid: widget.calling ? widget.receiverId : widget.callerId,
                      name: widget.contactName,
                      profilePhoto: widget.contactPhoto);

                  _sqliteMethods.addLogs(
                      contact, widget.calling ? "CALLED" : "GOTCALLED");
                } else {
                  //doesn't matter who called, but since you did not talked therefore this becomes a missed call
                  //for both the users
                  print(
                      "this is the else part where call duration is not greater than zero");
                  Message _message = Message(
                      receiverId: widget.receiverId,
                      senderId: widget.callerId,
                      message: "Call Missed",
                      timestamp: FieldValue.serverTimestamp(),
                      type: 'special');

                  _repository.addCallDetailsToDb(_message, widget.receiverId,
                      widget.callerId, widget.calling);

                  User contact = User(
                      uid: widget.callerId,
                      name: widget.contactName,
                      profilePhoto: widget.contactPhoto);

                  _sqliteMethods.addLogs(
                    contact,
                    "MISSED",
                  );
                }
                Navigator.pop(context);
                return Text("Rejected ...",
                    style: TextStyle(color: Colors.white));
                break;

              case CallState.INCOMING:
                audioPlayer.stop();
                return Text("Incoming ...",
                    style: TextStyle(color: Colors.white));

                break;

              case CallState.PICKED:
                audioPlayer.stop();
                return Container();
                break;

              case CallState.IDLE:
                audioPlayer.stop();
                return Container();
                break;

              default:
                return Text("Default ...",
                    style: TextStyle(color: Colors.white));
            }
          },
        ),
      ),
    );
  }

  /// Info panel to show logs
  Widget _panel() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 48),
        alignment: Alignment.bottomCenter,
        child: FractionallySizedBox(
          heightFactor: 0.5,
          child: Container(
              padding: EdgeInsets.symmetric(vertical: 48),
              child: ListView.builder(
                  reverse: true,
                  itemCount: _infoStrings.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (_infoStrings.length == 0) {
                      return null;
                    }
                    return Padding(
                        padding:
                            EdgeInsets.symmetric(vertical: 3, horizontal: 10),
                        child: Row(mainAxisSize: MainAxisSize.min, children: [
                          Flexible(
                              child: Container(
                                  padding: EdgeInsets.symmetric(
                                      vertical: 2, horizontal: 5),
                                  decoration: BoxDecoration(
                                    color: Colors.yellowAccent,
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Text(_infoStrings[index],
                                      style:
                                          TextStyle(color: Colors.blueGrey))))
                        ]));
                  })),
        ));
  }

  void _onCallEnd(BuildContext context) {
    _repository.endVideoCall(widget.receiverId, widget.callerId);
    audioPlayer.stop();
    bloc.rejected();
  }

  void _onToggleMute() {
    setState(() {
      muted = !muted;
    });
    AgoraRtcEngine.muteLocalAudioStream(muted);
  }

  void _onSwitchCamera() {
    AgoraRtcEngine.switchCamera();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Center(
            child: Stack(
          children: <Widget>[
            _viewRows(),
            _onScreenText(),
            _toolbar(),
          ],
        )));
  }
}
