import 'dart:async';

import 'package:audioplayer/audioplayer.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kudos/bloc/call_state_bloc.dart';
import 'package:kudos/models/message.dart';
import 'package:kudos/models/users.dart';
import 'package:kudos/resources/firebase_repository.dart';
import 'package:kudos/resources/sqlite_methods.dart';
import 'package:kudos/screens/callscreens/call.dart';
import 'package:kudos/theme/universal_colors.dart';
import 'package:kudos/utils/utilities.dart';

class PickupScreen extends StatefulWidget {
  final String receiverId;

  PickupScreen({
    @required this.receiverId,
  });

  @override
  _PickupScreenState createState() => _PickupScreenState();
}

class _PickupScreenState extends State<PickupScreen> {
  String songurl = "https://mobcup.net/d/o3drz01f/mp3";
  String channelName;

  String callerId;
  String callerName;
  String callerUsername;
  String callerPhoto;
  Utils utils;
  FirebaseRepository _repository = FirebaseRepository();
  AudioPlayer audioPlayer;
  SqliteMethods _sqliteMethods;

  @override
  void initState() {
    super.initState();
    _sqliteMethods = SqliteMethods();
    utils = Utils();
    initPlayer();
  }

  void initPlayer() {
    audioPlayer = AudioPlayer();
  }

  @override
  void dispose() {
    audioPlayer.stop();
    super.dispose();
  }

  bool laying = false;

  final CallBloc bloc = UniversalVariables.bloc;

  @override
  Widget build(BuildContext context) {
    _repository
        .getCallerDetails(widget.receiverId)
        .then((DocumentSnapshot doc) {
      setState(() {
        callerId = doc["call_by"];
        channelName = doc["channel_name"];
      });
    });

    return Scaffold(
      backgroundColor: UniversalVariables.blackColor,
      body: FutureBuilder(
        future: Firestore.instance.collection("users").document(callerId).get(),
        builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.data != null) {
            callerName = snapshot.data["name"];
            callerUsername = snapshot.data["username"];
            callerPhoto = snapshot.data["profile_photo"];

            return Align(
              alignment: Alignment.center,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 120,
                    width: 120,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(60)),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(60),
                      child: Image.network(
                        callerPhoto,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  SizedBox(height: 25),
                  Text(
                    callerName,
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Text(
                    callerUsername,
                    style: TextStyle(
                      color: Colors.grey[700],
                      fontSize: 20,
                    ),
                  ),
                  SizedBox(height: 100),
                  _toolbar(),
                ],
              ),
            );
          }

          return Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
        },
      ),
    );
  }

  Widget _toolbar() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          RawMaterialButton(
            onPressed: () => rejectCall(),
            child: new Icon(
              Icons.call_end,
              color: Colors.white,
              size: 35.0,
            ),
            shape: new CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.redAccent,
            padding: const EdgeInsets.all(15.0),
          ),
          RawMaterialButton(
            onPressed: () => pickCall(),
            child: new Icon(
              Icons.call,
              color: Colors.white,
              size: 35.0,
            ),
            shape: new CircleBorder(),
            elevation: 2.0,
            fillColor: Colors.green[500],
            padding: const EdgeInsets.all(15.0),
          ),
        ],
      ),
    );
  }

  rejectCall() {
    bloc.rejected();

     Message _message = Message(
                      receiverId: widget.receiverId,
                      senderId: callerId,
                      message: "Call Missed",
                      timestamp: FieldValue.serverTimestamp(),
                      type: 'special');

                  _repository.addCallDetailsToDb(_message, widget.receiverId,
                      callerId, false);
    
    if (callerName != null && callerPhoto != null) {
      User caller = User(
        uid: callerId,
        name: callerName,
        profilePhoto: callerPhoto
      );

      _sqliteMethods.addLogs(caller, "MISSED");
    }
    _repository.endVideoCall(widget.receiverId, callerId);
  }

  pickCall() {
    _repository.engageUsers(widget.receiverId, callerId);

    utils.handleCameraAndMicrophonePermissions().then((permissionGranted) {
      if (permissionGranted) {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CallPage(
                      callerId: callerId,
                      receiverId: widget.receiverId,
                      calling: false,
                      channelName: channelName,
                      contactName: callerName,
                      contactPhoto: callerPhoto,
                    )));
      }
    });
  }
}
