import 'package:flutter/material.dart';

class Call {
  int id;
  String receiverId;
  String receiverName;
  String status;
  String time;
  String receiverPhoto;
  Call({
    this.id,
    this.receiverId,
    this.receiverName,
    this.receiverPhoto,
    this.status,
    this.time,
  });

  Map<String, dynamic> toMap() {
    var callMap = <String, dynamic>{
      "id": id,
      "receiver_id": receiverId,
      "receiver_name": receiverName,
      "receiver_photo": receiverPhoto,
      "status": status,
      "time": time,
    };

    return callMap;
  }

  Call.fromMap(Map<String, dynamic> callMap) {
    this.id = callMap["id"];
    this.receiverId = callMap["receiver_id"];
    this.receiverName = callMap["receiver_name"];
    this.receiverPhoto = callMap["receiver_photo"];
    this.status = callMap["status"];
    this.time = callMap["time"];
  }
}
